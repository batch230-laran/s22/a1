// console.log("Hello World!");




/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];


let friendsList = [];


// ------------------------------------


function registerUser(name) {

	if (registeredUsers.includes(name)) {
		alert("Registration failed. Username already exists!");
	}

	else {
		registeredUsers.push(name);
		alert("Thank you for registering!");
	}
}




// ------------------------------------



function registerFriend(name) {
	if (registeredUsers.includes(name)) {
		friendsList.push(name);
		alert("You have added " + name + " as a friend!");
	}

	else {
		alert("User not found.");
	}
}




// ------------------------------------


function displayFriendsList() {
	if (friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	}

	else {
		friendsList.forEach(function(friend) {
			console.log(friend);
		})
	}
}






// ------------------------------------



function displayRegisteredFriends() {
	if (friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	}

	else {
		alert("You currently have friends.");
	}
}





// ------------------------------------


function deleteFriend() {
	friendsList.pop();
	if (friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	}
}







// ------------------------------------ STRETCH GOAL





function deleteSpecificUser() {
	friendsList.splice(2,3);
	if (friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	}
}



































